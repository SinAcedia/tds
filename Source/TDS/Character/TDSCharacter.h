// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLibrary/Types.h"
#include "../Weapon/WeaponDefault.h"
#include "../Character/TDSInventoryComponent.h"
#include "../Character/TDSCharacterHealthComponent.h"
#include "../Interface/TDS_IGameActor.h"
#include "../StateEffects/TDS_StateEffect.h"
#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	FORCEINLINE class UTDSInventoryComponent* GetInventoryComponent() const { return InventoryComponent; }
	FORCEINLINE class UTDSCharacterHealthComponent* GetCharHealthComponent() const { return CharHealthComponent; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
	class UTDSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UTDSCharacterHealthComponent* CharHealthComponent;

public:

	FTimerHandle TimerHandle_RagDollTimer;
	//Weapon
	AWeaponDefault* CurrentWeapon = nullptr;

	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	//TSubclassOf<AWeaponDefault> InitWeaponClass = nullptr;
	FName InitWeaponName;

	//Cursor
	UDecalComponent* CurrentCursor = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Movement
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Movement")
	FCharacterSpeed MovementSpeedInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;
	bool bIsMoving = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DeadState")
	bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DeadState")
	TArray<UAnimMontage*> DeadsAnim;

	//Ability
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UTDS_StateEffect> AbilityEffect;
	//Effect
	TArray<UTDS_StateEffect*> Effects;
	//Stamina
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Physical")
	float Stamina = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Test")
	float LoseStaminaSpeed = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Test")
	float RestStaminaSpeed = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Test")
	float TakeAccelerationSpeed = 450.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Test")
	float LoseAccelerationSpeed = 100.f;

	//Camera
	float AxisWheel = 90.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Camera")
	float CurrentHeight = 1000.0f;
	float TargetHeight = CurrentHeight;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Camera")
	float MaxHeight = 1200.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Camera")
	float MinHeight = 600.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Camera")
	float ZoomScale = 100.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Camera")
	bool IsZoom = false;


	//Inputs
	float AxisX = 0.0f;
	float AxisY = 0.0f;
	UFUNCTION()
	void InputAxisX(float value);
	UFUNCTION()
	void InputAxisY(float value);
	UFUNCTION()
	void InputWheel(float value);
	UFUNCTION()
	void InputRMBPressed();
	UFUNCTION()
	void InputRMBReleased();
	UFUNCTION()
	void InputWalkPressed();
	UFUNCTION()
	void InputWalkReleased();
	UFUNCTION()
	void InputSprintPressed();
	UFUNCTION()
	void InputSprintReleased();
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();

	//Tick Function
	UFUNCTION()
	void MovementTick(float DeltaTime);

	//Func
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo
		, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
	void RemoveCurrentWeapon();

	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	//Inventory Func
	void TrySwicthNextWeapon();
	void TrySwitchPreviosWeapon();

	//Ability Func
	UFUNCTION(BlueprintCallable)
	void TryAbilityEnabled();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;

	//Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTDS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDS_StateEffect* RemoveEffect) override;
	void AddEffect(UTDS_StateEffect* NewEffect) override;
	//End Interface

	UFUNCTION()
	void CharDead();
	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent
		, class AController* EventInstigator, AActor* DamageCauser) override;



	UFUNCTION()
	void UpdateStamina(float DeltaSecond);
	UFUNCTION()
	void SpeedAcceleration(float DeltaSecond);
	UFUNCTION(BlueprintCallable)
	void Zoom(float MaxValue, float MinValue, float DeltaTime, bool newIsZoom);
};