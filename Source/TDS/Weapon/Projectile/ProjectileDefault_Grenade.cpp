// Fill out your copyright notice in the Description page of Project Settings.


#include "../Projectile/ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include <DrawDebugHelpers.h>

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TDS.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);


void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp
, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation()
			, ProjectileSettings.ProjectileRadiusMaxDamage, 8, FColor::Red, false, 4.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation()
			, ProjectileSettings.ProjectileRadiusMinDamage, 8, FColor::Yellow, false, 4.0f);
	}

	TimerEnabled = false;
	if (ProjectileSettings.ExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSettings.ExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.ExplodeSound, GetActorLocation());
	}
	
	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSettings.ExplodeMaxDamage,
		ProjectileSettings.ExplodeMinDamage,
		GetActorLocation(),
		ProjectileSettings.ProjectileRadiusMinDamage,
		ProjectileSettings.ProjectileRadiusMaxDamage,
		ProjectileSettings.ExplodeFalloffCoef,
		NULL, IgnoredActor, this, nullptr);

	this->Destroy();
}
