// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "../FuncLibrary/Types.h"
#include "Projectile/ProjectileDefault.h"
#include "WeaponDefault.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class UArrowComponent* ShellLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		FWeaponInfo WeaponSettings;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AddicionalWeaponInfo")
		FAdditionalWeaponInfo AdditionalWeaponInfo;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	/*
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponStateLogic")
		bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponStateLogic")
		bool WeaponReloading = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponStateLogic")
		bool WeaponAiming = false;
	*/
	//flags
	bool WeaponFiring = false;
	bool WeaponReloading = false;
	bool WeaponAiming = false;

	bool BlockFire = false;
	bool DropShellFlag = false;
	bool DropClipFlag = false;
	//Dispersion
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	FVector ShootEndLocation = FVector(0);

	//Timers
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropLogic")
		float DropShellTimer = -1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropLogic")
		float DropClipTimer = -1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.0f;

	// Called every frame
		virtual void Tick(float DeltaTime) override;

		void FireTick(float DeltaTime);
		void ReloadTick(float DeltaTime);
		void DispersionTick(float DeltaTime);
		void ClipDropTick(float DeltaTime);
		void ShellDropTick(float DeltaTime);
		void WeaponInit();

	UFUNCTION(BlueprintCallable)
		void SetWeaponStateFire(bool bIsFire);
	
		bool CheckWeaponCanFire();
		FProjectileInfo GetProjectile();
		void Fire();
		void UpdateStateWeapon(EMovementState NewMovementState);
		void ChangeDispersionByShot();
		float GetCurrentDispersion() const;
		FVector ApplyDispersionToShoot(FVector DirectionShoot)const;

		FVector GetFireEndLocation()const;
		int8 GetNumberProjectileByShot() const;

	UFUNCTION(BlueprintCallable)
		int32 GetWeaponRound();
	UFUNCTION()
		void InitReload();
		void FinishReload();
		void CancelReload();
		bool CheckCanWeaponReload();
		int8 GetAvailableAmmoForReload();
		

	UFUNCTION()
		void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset
			, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion
			, float PowerImpulse, float CustomMass);


};
