// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StateEffect.h"
#include "../Interface/TDS_IGameActor.h"
#include "Kismet/GameplayStatics.h"


bool UTDS_StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}
	
	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);
		

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect
			, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator
			, EAttachLocation::SnapToTarget, false);
	}

	return true;
}


void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}

bool UTDS_StateEffect_Invincible::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	if (myActor)
	{
		myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			ActualCoefDamage = myHealthComp->CoefDamage;
			myHealthComp->CoefDamage = 0;
		}
	}
	return true;
}

void UTDS_StateEffect_Invincible::DestroyObject()
{
	if (myHealthComp)
	{
		myHealthComp->CoefDamage = ActualCoefDamage;
	}
	Super::DestroyObject();
}

bool UTDS_StateEffect_DestroyOnTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_DestroyTimer, this, &UTDS_StateEffect_DestroyOnTimer::DestroyObject, Timer, false);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect
			, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator
			, EAttachLocation::SnapToTarget, false);
	}

	return true;
}

void UTDS_StateEffect_DestroyOnTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

bool UTDS_StateEffect_BoostHP::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	if (myActor)
	{
		myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->SetMaxHealth(myHealthComp->GetMaxHealth() + Power);
			myHealthComp->SetCurrentHealth(myHealthComp->GetCurrentHealth() + Power);
		}
	}

	return true;
}

void UTDS_StateEffect_BoostHP::DestroyObject()
{
	if (myHealthComp)
	{
		myHealthComp->SetMaxHealth( myHealthComp->GetMaxHealth() - Power);
	}
	Super::DestroyObject();
}

bool UTDS_StateEffect_Stun::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	if (myActor)
	{
		myChar = Cast<ACharacter>(myActor);
		if (myChar)
		{
			myController = myChar->GetController()->CastToPlayerController();
			myChar->Controller = nullptr;
			if (StunAnim)
			{
				myChar->GetMesh()->GetAnimInstance()->Montage_Play(StunAnim);
			}
		}
	}

	return true;
}

void UTDS_StateEffect_Stun::DestroyObject()
{
	if (myChar && myController)
	{
		myChar->GetMesh()->GetAnimInstance()->StopAllMontages(0.15f);
		myChar->Controller = myController;
		myController = nullptr;
		myChar = nullptr;
	}
	Super::DestroyObject();
}
/*
bool UTDS_StateEffect_ElemDamage::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	myVector = myActor->GetActorLocation();
	return true;
}

void UTDS_StateEffect_ElemDamage::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ElemDamage::Execute()
{
	
	//UGameplayStatics::ApplyRadialDamage(GetWorld(), Power, myActor->GetActorLocation()
	//	, RadiusDamage, NULL, IgnoredActor, myActor);
}*/
